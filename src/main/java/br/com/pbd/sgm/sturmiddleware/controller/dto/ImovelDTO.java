package br.com.pbd.sgm.sturmiddleware.controller.dto;

import br.com.pbd.sgm.sturmiddleware.service.Imovel;

import java.util.List;
import java.util.stream.Collectors;

public class ImovelDTO {

    private final String numeroInscricao;
    private final String endereco;
    private final String bairro;
    private final String cep;
    private final String cidade;
    private final String uf;
    private final String area;
    private final TipoImovel tipoImovel;

    public ImovelDTO(Imovel imovel) {
        this.numeroInscricao = imovel.getNumeroInscricao();
        this.endereco = formatarEndereco(imovel);
        this.bairro = imovel.getBairro();
        this.cep = imovel.getCep();
        this.cidade = imovel.getCidade();
        this.uf = imovel.getUf();
        this.area = imovel.getAreaM2();
        this.tipoImovel = TipoImovel.findByCodigo(imovel.getTipo());
    }

    public String getNumeroInscricao() {
        return numeroInscricao;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public String getCep() {
        return cep;
    }

    public String getCidade() {
        return cidade;
    }

    public String getUf() {
        return uf;
    }

    public String getArea() {
        return area;
    }

    public TipoImovel getTipoImovel() {
        return tipoImovel;
    }

    private String formatarEndereco(Imovel imovel) {
        return String.format("%s, %s", imovel.getLogradouro(), imovel.getNumero());
    }

    public static List<ImovelDTO> toList(List<Imovel> imoveis) {
        return imoveis.stream().map(ImovelDTO::new).collect(Collectors.toList());
    }
}
