package br.com.pbd.sgm.sturmiddleware.controller.dto;

import br.com.pbd.sgm.sturmiddleware.service.DebitoImovel;

import java.util.List;
import java.util.stream.Collectors;

public class DebitoImovelDTO {

    private String id;
    private String inscricaoImovel;
    private String tipoDebito;
    private String ano;
    private String valor;
    private String multa;
    private String juros;
    private String outros;
    private String situacao;

    public DebitoImovelDTO(DebitoImovel debitoImovel) {
        this.id = debitoImovel.getId();
        this.inscricaoImovel = debitoImovel.getInscricaoImovel();
        this.tipoDebito = debitoImovel.getTipoDebito();
        this.ano = debitoImovel.getAno();
        this.valor = debitoImovel.getValor();
        this.multa = debitoImovel.getMulta();
        this.juros = debitoImovel.getJuros();
        this.outros = debitoImovel.getOutros();
        this.situacao = debitoImovel.getSituacao();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInscricaoImovel() {
        return inscricaoImovel;
    }

    public void setInscricaoImovel(String inscricaoImovel) {
        this.inscricaoImovel = inscricaoImovel;
    }

    public String getTipoDebito() {
        return tipoDebito;
    }

    public void setTipoDebito(String tipoDebito) {
        this.tipoDebito = tipoDebito;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getMulta() {
        return multa;
    }

    public void setMulta(String multa) {
        this.multa = multa;
    }

    public String getJuros() {
        return juros;
    }

    public void setJuros(String juros) {
        this.juros = juros;
    }

    public String getOutros() {
        return outros;
    }

    public void setOutros(String outros) {
        this.outros = outros;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public static List<DebitoImovelDTO> toList(List<DebitoImovel> debitos) {
        return debitos.stream().map(DebitoImovelDTO::new).collect(Collectors.toList());
    }
}
