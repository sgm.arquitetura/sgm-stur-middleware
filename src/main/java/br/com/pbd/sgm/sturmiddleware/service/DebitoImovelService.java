package br.com.pbd.sgm.sturmiddleware.service;

import br.com.pbd.sgm.sturmiddleware.controller.dto.DebitoImovelDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class DebitoImovelService {

    private final RestTemplate client;

    public DebitoImovelService(RestTemplate client) {
        this.client = client;
    }

    public List<DebitoImovelDTO> buscarPorInscricaoImovel(String inscricao) throws JsonProcessingException {
        String urlBase = "http://localhost:3000";
        String uri = String.format("%s/debitosImoveis?inscricaoImovel_like=%s", urlBase, inscricao);

        String json = client.getForObject(uri, String.class);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.reader()
                .forType(new TypeReference<List<DebitoImovel>>() {})
                .readValue(json);
    }
}
