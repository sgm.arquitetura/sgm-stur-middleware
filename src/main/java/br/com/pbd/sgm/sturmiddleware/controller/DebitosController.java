package br.com.pbd.sgm.sturmiddleware.controller;

import br.com.pbd.sgm.sturmiddleware.controller.dto.DebitoImovelDTO;
import br.com.pbd.sgm.sturmiddleware.service.DebitoImovelService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/stur/debitos")
public class DebitosController {

    private final DebitoImovelService debitoImovelService;

    public DebitosController(DebitoImovelService debitoImovelService) {
        this.debitoImovelService = debitoImovelService;
    }

    @GetMapping("/{inscricaoImovel}")
    public List<DebitoImovelDTO> buscarPorInscricaoImovel(@PathVariable String inscricaoImovel) throws JsonProcessingException {
        return debitoImovelService.buscarPorInscricaoImovel(inscricaoImovel);
    }
}
