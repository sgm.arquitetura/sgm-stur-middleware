package br.com.pbd.sgm.sturmiddleware.service;

import java.util.List;

public class Imovel {

    private Long id;
    private String numeroInscricao;
    private List<String> proprietarios;
    private String logradouro;
    private String bairro;
    private String numero;
    private String cep;
    private String cidade;
    private String uf;
    private String areaM2;
    private String tipo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroInscricao() {
        return numeroInscricao;
    }

    public void setNumeroInscricao(String numeroInscricao) {
        this.numeroInscricao = numeroInscricao;
    }

    public List<String> getProprietarios() {
        return proprietarios;
    }

    public void setProprietarios(List<String> proprietarios) {
        this.proprietarios = proprietarios;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getAreaM2() {
        return areaM2;
    }

    public void setAreaM2(String areaM2) {
        this.areaM2 = areaM2;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
