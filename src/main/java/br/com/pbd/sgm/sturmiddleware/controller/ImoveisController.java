package br.com.pbd.sgm.sturmiddleware.controller;

import br.com.pbd.sgm.sturmiddleware.controller.dto.ImovelDTO;
import br.com.pbd.sgm.sturmiddleware.service.ImovelService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/stur/imoveis")
public class ImoveisController {


    private final ImovelService imoveisService;

    public ImoveisController(ImovelService imoveisService) {
        this.imoveisService = imoveisService;
    }

    @GetMapping("/{cpf}")
    public List<ImovelDTO> buscarPorCpfProprietario(@PathVariable String cpf) throws JsonProcessingException {
        return ImovelDTO.toList(imoveisService.buscarPorCpfProprietario(cpf));
    }

}
