package br.com.pbd.sgm.sturmiddleware.controller.dto;

import java.util.Arrays;

public enum TipoImovel {

    URBANO("U"),
    RURAL("R");

    private final String codigo;

    TipoImovel(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    static TipoImovel findByCodigo(String codigo) {
        return Arrays.stream(TipoImovel.values())
                .filter(t -> t.getCodigo().equalsIgnoreCase(codigo))
                .findFirst().orElse(null);
    }
}
