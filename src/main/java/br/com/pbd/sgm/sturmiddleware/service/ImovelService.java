package br.com.pbd.sgm.sturmiddleware.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class ImovelService {

    private final RestTemplate client;

    public ImovelService(RestTemplate client) {
        this.client = client;
    }

    public List<Imovel> buscarPorCpfProprietario(String cpf) throws JsonProcessingException {
        String urlBase = "http://localhost:3000";
        String uri = String.format("%s/imoveis?proprietarios_like=%s", urlBase, cpf);

        String json = client.getForObject(uri, String.class);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.reader()
                .forType(new TypeReference<List<Imovel>>() {})
                .readValue(json);
    }

}
