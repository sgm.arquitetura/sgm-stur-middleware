package br.com.pbd.sgm.sturmiddleware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class SturMiddlewareApplication {

    public static void main(String[] args) {
        SpringApplication.run(SturMiddlewareApplication.class, args);
    }


    @Bean
    public RestTemplate newRestTemplate(){
        return new RestTemplate();
    }
}
